# HFrEF Clustering Validation

Version 0.9.0 - [![License](https://img.shields.io/badge/license-CC--BY--SA--4.0-orange)](https://choosealicense.com/licenses/cc-by-sa-4.0)

--------------

[[_TOC_]]


## Project organization

```
.
├── .gitignore
├── CITATION.md
├── LICENSE.md
├── README.md
├── requirements.txt
├── data               <- Files necessary to process the data and apply the models
│   └── example        <- Example data to illustrate the validation
├── results            <- Example results of the validation using the example data
└── src                <- Source code for this project

```


## Installation

The required R-packages can be installed using conda:
```
conda create --name <env> --file requirements.txt
```


## Usage

The validation or application of the derived model to an example dataset is performed using
the [`validation example`](/src/example_validation.ipynb). This example can be easily adapted.

### The models

The clinical and protein models are saved in `data/clinical_lca_model.rds` and `data/protein_lca_model.rds`.
The PCA that is applied to the QC-ed protein measurements is saved in `data/protein_pca_data.rds`.

### Required data

#### Clinical data
Clinical clustering was performed using the following categorical variables:

|Name|Description|Categories|
|---|---|---|
|CAD_E1_C12|History of CAD|No / Uknown / Yes|
|ARRHYTH_E1_C12|History of arrhythmias|No / Yes|
|HT_E1_C13|History of hypertension|No / Yes|
|SMOKING_E1_C13|Ever smoked|No / Yes|
|Age_cat|Age category|<65 / 65-80 / >80|
|Etiology|Etiology of HF|Cardiomyopathy / Exposure_toxic_substances / Hypertension / Infiltrative_disease / Ischemic_heart_disease / Myocarditis / Unknown / Valvular_disease|

The variables should be formatted in the following way:
- The column names should be as listed in the `Name` column of the table above
- The categories of the variables should be as listed in the `Categories` column of the table above.

#### Proteomics data
The protein data should be formatted in the following way:
- the rows are individuals
- the columns are protein measurements
- the names of the proteins used in the analysis are saved in `data/proteins.tsv`

#### Association with outcomes
Survival analyses were conducted to identify differences in time to the following events:

|Name|Defined as|Time column|
|---|---|---|
|major_cardiovascular_event|composite of HF hospitalization, cardiovascular death, heart transplantation, and left ventricular assist device implantation|time_major_cardiovascular_event|
|hf_hospitalization|Heart failure hospitalization|time_hf_hospitalization|
|cardiovascular_death|Death due to cadiovascular cause|time_cardiovascular_death|
|all_cause_mortality|All-cause mortality|time_all_cause_mortality|

The columns with the event should be coded `0 = no event reached` and `1 = event reached`. The time to event column should be in years.

#### Examples
See for example data the files `data/example/clinical_data_example.tsv`, `data/example/protein_data_example.tsv`, and `data/example/outcomes_example.tsv`.


## License

This project is licensed under the terms of the [MIT License](/LICENSE.md)


## Citation

Please cite this project as described [here](/CITATION.md).
